<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage OCPL_New
 * @since OCPL New WP Theme 1.0
 */
get_header(); ?>
<section id="home">
	<div class="home-pattern"></div>
	<div id="main-carousel" class="carousel slide" data-ride="carousel"> 
		<ol class="carousel-indicators">
			<li data-target="#main-carousel" data-slide-to="0" class="active"></li>
			<li data-target="#main-carousel" data-slide-to="1"></li>
			<li data-target="#main-carousel" data-slide-to="2"></li>
		</ol><!--/.carousel-indicators--> 
		<div class="carousel-inner">
			<?php $ocpl_slider = new WP_Query(array(
				'post_type' => 'ocpl_slider',
				'posts_per_page' => 10
			)); 
				$i=0; while($ocpl_slider->have_posts()) : $ocpl_slider->the_post(); 
				$slider_info = get_post_meta( get_the_ID(), 'slider_info', true );
				$slider_button = get_post_meta( get_the_ID(), 'slider_button', true );
			?>
				<div class="item <?php if($i==0){ echo 'active'; } ?>" style="background-image: url(<?php the_post_thumbnail_url(); ?>)"> 
					<div class="carousel-caption"> 
						<div> 
							<h2 class="heading animated bounceInDown"><?php the_title(); ?></h2> 
							<p class="animated bounceInUp"><?php echo $slider_info; ?></p> 
							<a class="btn btn-default slider-btn animated fadeIn" href="<?php the_permalink(); ?>"><?php echo $slider_button; ?></a> 
						</div> 
					</div> 
				</div>
			<?php $i++; endwhile; ?>
	</div><!--/.carousel-inner-->

	<a class="carousel-left member-carousel-control hidden-xs" href="#main-carousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
	<a class="carousel-right member-carousel-control hidden-xs" href="#main-carousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
	</div> 

</section><!--/#home-->

<section id="about-us">
<div class="container">
<div class="text-center">
	<div class="col-sm-8 col-sm-offset-2">
		<h2 class="title-one">Why With Us?</h2>
		<?php $whyWithUs = get_page_by_path( 'why-with-us' ); ?>
		<p><?php echo $whyWithUs->post_content; ?></p>
	</div>
</div>
<div class="about-us">
	<div class="row">
		<div class="col-sm-6">
			<h3>Why with us?</h3>
			<ul class="nav nav-tabs">
				<li class="active"><a href="#about" data-toggle="tab"><i class="fa fa-chain-broken"></i> About</a></li>
				<li><a href="#mission" data-toggle="tab"><i class="fa fa-th-large"></i> Mission</a></li>
				<li><a href="#vision" data-toggle="tab"><i class="fa fa-users"></i> Vision</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane fade in active" id="about">
					<div class="media">
						<?php 
						$aboutUs = get_page_by_path( 'about-us' ); 
						if (has_post_thumbnail( $aboutUs->ID ) ):
						 $about_img = wp_get_attachment_image_src( get_post_thumbnail_id( $aboutUs->ID ), 'single-post-thumbnail' ); ?>
						  <img style="height: 160px;" class="pull-left media-object" src="<?php echo $about_img[0]; ?>" alt="about us">
						<?php endif; ?>
						<div class="media-body">
							<p><?php echo $aboutUs->post_content; ?></p>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="mission">
				<?php $mission = get_page_by_path( 'mission' ); ?>
					<div class="media">
						<?php if (has_post_thumbnail( $mission->ID ) ): ?>
						  <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $mission->ID ), 'single-post-thumbnail' ); ?>
						  <img style="height: 160px;" class="pull-left media-object" src="<?php echo esc_url($image[0]); ?>" alt="Mission">
						<?php endif; ?>
						<div class="media-body">
							<p><?php echo $mission->post_content; ?></p>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="vision">
					<div class="media">
						<?php 
						$vision = get_page_by_path( 'vision' ); 
						if (has_post_thumbnail( $vision->ID ) ):
						 $vision_img = wp_get_attachment_image_src( get_post_thumbnail_id( $vision->ID ), 'single-post-thumbnail' ); ?>
						  <img style="height: 160px;" class="pull-left media-object" src="<?php echo $vision_img[0]; ?>" alt="about us">
						<?php endif; ?>
						<div class="media-body">
							<p><?php echo $vision->post_content; ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<h3>Our Skills</h3>
			<div class="skill-bar">
				<div class="skillbar clearfix " data-percent="99%">
					<div class="skillbar-title">
						<span>HTML5 &amp; CSS3</span>
					</div>
					<div class="skillbar-bar"></div>
					<div class="skill-bar-percent">99%</div>
				</div> <!-- End Skill Bar -->
				<div class="skillbar clearfix" data-percent="85%">
					<div class="skillbar-title"><span>UI Design</span></div>
					<div class="skillbar-bar"></div>
					<div class="skill-bar-percent">85%</div>
				</div> <!-- End Skill Bar -->
				<div class="skillbar clearfix " data-percent="85%">
					<div class="skillbar-title"><span>jQuery</span></div>
					<div class="skillbar-bar"></div>
					<div class="skill-bar-percent">85%</div>
				</div> <!-- End Skill Bar -->
				<div class="skillbar clearfix " data-percent="90%">
					<div class="skillbar-title"><span>PHP</span></div>
					<div class="skillbar-bar"></div>
					<div class="skill-bar-percent">90%</div>
				</div> <!-- End Skill Bar -->
				<div class="skillbar clearfix " data-percent="95%">
					<div class="skillbar-title"><span>Laravel</span></div>
					<div class="skillbar-bar"></div>
					<div class="skill-bar-percent">95%</div>
				</div> <!-- End Skill Bar --></div>
			</div>
		</div>
	</div>
</div>
</section><!--/#about-us-->

<section id="services" class="parallax-section">
<div class="container">
	<div class="row text-center">
		<div class="col-sm-8 col-sm-offset-2">
			<h2 class="title-one">Services</h2>
			<?php $services = get_page_by_path( 'services' ); ?>
			<p><?php echo $services->post_content; ?></p>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="our-service">
				<div class="services row">
					<?php $services = new WP_Query(array(
						'post_type' => 'ocpl_services',
						'posts_per_page' => 3
					)); ?>
					<?php $i=0; while($services->have_posts()) : $services->the_post(); ?>
						<div class="col-sm-4">
							<div class="single-service">
								<?php if($i==0){ echo '<i class="fa fa-th"></i>'; } 
								else if($i==1) {echo '<i class="fa fa-html5"></i>';} 
								else if($i==2) {echo '<i class="fa fa-users"></i>';} 
								else { echo '<i class="fa fa-html5"></i>'; } ?>
								<h2><?php the_title(); ?></h2>
								<?php the_content(); ?>
							</div>
						</div>
					<?php $i++; endwhile; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section><!--/#service-->

<section id="our-team">
	<div class="container">
		<div class="row text-center">
			<div class="col-sm-8 col-sm-offset-2">
				<h2 class="title-one">MEET OUR TEAM</h2>
				<!-- <?php //$ocplTeam = get_page_by_path( 'ocpl-team' ); ?>
				<p><?php //echo $ocplTeam->post_content; ?></p> -->
			</div>
		</div>
		<div id="team-carousel" class="carousel slide" data-interval="false">
			<a class="member-left" href="#team-carousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
			<a class="member-right" href="#team-carousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
			<div class="carousel-inner team-members">
				<?php $team = new WP_Query(array(
				 	'post_type' => 'ocpl_team'
				 )); ?>
				 
				<div class="row item active">
					<?php $i=1; while($team->have_posts()) : $team->the_post();
						$position = get_post_meta( get_the_ID(), 'position', true );

						$fb_url = get_post_meta( get_the_ID(), 'fb_url', true );
						$twitter_url = get_post_meta( get_the_ID(), 'twitter_url', true );
						$google_plus_url = get_post_meta( get_the_ID(), 'google_plus_url', true );
						$dribbble_url = get_post_meta( get_the_ID(), 'dribbble_url', true );
						$linkedin_url = get_post_meta( get_the_ID(), 'linkedin_url', true );
						if($i%5==0){ $i=1; echo '</div><div class="row item">'; }
					 ?>
						<div class="col-sm-6 col-md-3">
							<div class="single-member">
								<img src="<?php the_post_thumbnail_url(); ?>" alt="team member" />
								<h4><?php the_title(); ?></h4>
								<h5><?php echo $position; ?></h5>
								<p><?php //the_content(); ?></p>
								<!-- <div class="socials">
									<a target="_blank" href="<?php //echo $fb_url; ?>"><i class="fa fa-facebook"></i></a>
									<a target="_blank" href="<?php //echo $twitter_url; ?>"><i class="fa fa-twitter"></i></a>
									<a target="_blank" href="<?php //echo $google_plus_url; ?>"><i class="fa fa-google-plus"></i></a>
									<a target="_blank" href="<?php //echo $dribbble_url; ?>"><i class="fa fa-dribbble"></i></a>
									<a target="_blank" href="<?php //echo $linkedin_url; ?>"><i class="fa fa-linkedin"></i></a>
								</div> -->
							</div>
						</div>
					<?php $i++; endwhile; ?>
				</div>
			</div>
		</div>
	</div>
</section><!--/#Our-Team-->

<section id="portfolio">
	<div class="container">
		<div class="row text-center">
			<div class="col-sm-8 col-sm-offset-2">
				<h2 class="title-one">Portfolio</h2>
				<!-- <?php //$portfolio = get_page_by_path( 'portfolio' ); ?>
				<p><?php //echo $portfolio->post_content; ?></p> -->
			</div>
		</div>
		<ul class="portfolio-filter text-center">
			<li><a class="btn btn-default active" href="#" data-filter="*">All</a></li>
			<li><a class="btn btn-default" href="#" data-filter=".codeigniter">CodeIgniter</a></li>
			<li><a class="btn btn-default" href="#" data-filter=".laravel">Laravel</a></li>
		</ul><!--/#portfolio-filter-->
		<div class="portfolio-items">
			<?php $team = new WP_Query(array(
			 	'post_type' => 'ocpl_portfolio'
			 )); ?>
			 <?php $i=1; while($team->have_posts()) : $team->the_post();
				$live_url = get_post_meta( get_the_ID(), 'live_url', true );
				$project_type = get_post_meta( get_the_ID(), 'project_type', true );
			 ?>
			 	<div class="col-sm-3 col-xs-12 portfolio-item <?php echo $project_type; ?>">
					<div class="view efffect">
						<div class="portfolio-image">
							<img src="<?php the_post_thumbnail_url(); ?>" alt="">
						</div> 
						<div class="mask text-center">
							<h3><?php the_title(); ?></h3>
							<h4><?php echo trunck_string(get_the_content(),50,true); ?></h4>
							<a href="<?php echo $live_url; ?>" alt="<?php echo $live_url; ?>" target="_blank"><i class="fa fa-link"></i></a>
							<a href="<?php the_post_thumbnail_url(); ?>" data-gallery="prettyPhoto"><i class="fa fa-search-plus"></i></a>
						</div>
					</div>
				</div>
			 <?php $i++; endwhile; ?>
			</div>
		</div> 
	</section> <!--/#portfolio-->

			<section id="clients" class="parallax-section">
				<div class="container">
					<div class="clients-wrapper">
						<div class="row text-center">
							<div class="col-sm-8 col-sm-offset-2">
								<h2 class="title-one">Our Clients</h2>
								<!-- <?php //$clientsUs = get_page_by_path( 'clients-say-about-us' ); ?>
								<p><?php //echo $clientsUs->post_content; ?></p> -->
							</div>
						</div>
						<div id="clients-carousel" class="carousel slide" data-ride="carousel"> <!-- Indicators -->
							<?php $team = new WP_Query(array(
								 	'post_type' => 'ocpl_testimonials'
								 )); ?>
							<ol class="carousel-indicators">
								<?php for($i=0;$i<count($team);$i++){ ?>
								<li data-target="#clients-carousel" data-slide-to="<?php echo $i; ?>" class="<?php if($i==0){ echo 'active'; } ?>"></li>
								<?php } ?>
							</ol> <!-- Wrapper for slides -->
							<div class="carousel-inner">								
								 <?php $i=1; while($team->have_posts()) : $team->the_post();
									$testimonial_url = get_post_meta( get_the_ID(), 'testimonial_url', true );
								 ?>
								<div class="item <?php if($i==1){ echo 'active'; } ?>">
									<div class="single-client">
										<div class="media">
											<img style="width: 165px;" class="pull-left" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
											<div class="media-body">
												<blockquote><p><?php echo trunck_string(get_the_content(),150,true); ?></p><small><?php the_title(); ?></small><a href="<?php echo $testimonial_url; ?>" target="_blank"><?php echo $testimonial_url; ?></a></blockquote>
											</div>
										</div>
									</div>
								</div>
								<?php $i++; endwhile; ?>
							</div>
						</div>
					</div>
				</div>
			</section><!--/#clients-->

			<section id="career"> 
				<div class="container">
					<div class="row text-center clearfix">
						<div class="col-sm-8 col-sm-offset-2">
							<h2 class="title-one">Career</h2>
							<!-- <p class="blog-heading">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p> -->
						</div>
					</div> 
					<div class="row text-left clearfix">
						<div class="col-sm-8 col-sm-offset-2">
                            <?php $ocpl_career = get_page_by_path( 'ocpl-career' ); ?>
							<?php echo $ocpl_career->post_content; ?>
                        </div>
					</div> 
				</div> 
			</section> <!--/#blog-->

				<section id="contact">
					<div class="container">
						<div class="row text-center clearfix">
							<div class="col-sm-8 col-sm-offset-2">
								<div class="contact-heading">
									<h2 class="title-one">Contact With Us</h2>
									<!-- <p>Online Content Provider Ltd</p> -->
								</div>
							</div>
						</div>
					</div>
					<div class="container">
						<div class="contact-details">
							<div class="pattern"></div>
							<div class="row text-center clearfix">
								<div class="col-sm-6">
									<div class="contact-address"><address><p><span>Online Content</span> Provider Ltd. (OCPL)</p>
									<div><small>(A concern of Business Automation Ltd.)</small></div>
									<strong>Road No-13, Avenue -2,<br> House no-1325,<br> Mirpur DOHS, Dhaka-1216.</strong><br><small>( ocpl_team@batworld.com , +880 2-9134510 )</small></address>
										<div class="social-icons">
											<a target="_blank" href="https://www.facebook.com/teamOcpl/"><i class="fa fa-facebook"></i></a>
											<a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
											<a target="_blank" href="#"><i class="fa fa-google-plus"></i></a>
											<a target="_blank" href="#"><i class="fa fa-linkedin"></i></a>
										</div>
									</div>
								</div>
								<div class="col-sm-6"> 
									<div id="contact-form-section">
									<?php echo do_shortcode( '[contact-form-7 id="313" title="CONTACT WITH US"]' ); ?>
										<!-- <div class="status alert alert-success" style="display: none"></div>
										<form id="contact-form" class="contact" name="contact-form" method="post" action="<?php //echo home_url('/send-email/'); ?>">
											<div class="form-group">
												<input type="text" name="sender_name" class="form-control name-field" required="required" placeholder="Your Name"></div>
												<div class="form-group">
													<input type="email" name="email" class="form-control mail-field" required="required" placeholder="Your Email">
												</div> 
												<div class="form-group">
													<textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Message"></textarea>
												</div> 
												<div class="form-group">
													<button type="submit" class="btn btn-primary">Send</button>
												</div>
											</form> 
										</div> -->
									</div>
								</div>
							</div>
						</div> 
					</section> <!--/#contact--> 
					<?php get_footer(); ?>