<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage OCPL_New
 * @since OCPL New WP Theme 1.0
 */
get_header(); ?>

<section id="contact">
	<?php while ( have_posts() ) : the_post(); ?>
	<div class="container">
		<div class="row text-center clearfix">
			<div class="col-sm-8 col-sm-offset-2">
				<div class="contact-heading">
					<h2 class="title-one"><?php the_title(); ?></h2>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="contact-details-">
			<div class="pattern"></div>
			<div class="row text-center clearfix">
				<div class="col-sm-12">
					<div class="page-content"><?php the_content(); ?></div>
				</div>
			</div>
		</div>
	</div> 
<?php endwhile; ?>
</section> <!--/#contact--> 
<?php get_footer(); ?>